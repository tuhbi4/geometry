﻿using System;
using static Geometry.Game;
using static Geometry.Validator;

namespace Geometry
{
    public class Board
    {
        public int Cols { get; }
        public int Rows { get; }
        private int Width { get; }
        private int Height { get; }
        public string Filler { get; }
        public int EmptyCellsCount { get; set; }
        public string[,] BoardMatrix { get; set; }
        private static readonly int borderSize = 2;

        public Board(int rows, int cols, string filler)
        {
            Cols = cols;
            Width = cols + 2 * borderSize;
            Rows = rows;
            Height = rows + 2 * borderSize;
            Filler = filler;
            EmptyCellsCount = cols * rows;
        }

        public void FillBoardMatrix()
        {
            BoardMatrix = new string[Height, Width];
            for (int rowIndex = 0; rowIndex < Height; rowIndex++)
            {
                for (int colIndex = 0; colIndex < Width; colIndex++)
                {
                    BoardMatrix[rowIndex, colIndex] = Filler;
                    if (colIndex == 0 || colIndex == Width - 1)
                    {
                        if (rowIndex <= 10) { BoardMatrix[rowIndex, colIndex] = $"|{rowIndex - 1}-|"; }
                        else { BoardMatrix[rowIndex, colIndex] = $"|{rowIndex - 1}|"; }
                    }
                    if (rowIndex == 0 || rowIndex == Height - 1)
                    {
                        if (colIndex <= 10) { BoardMatrix[rowIndex, colIndex] = $"|{colIndex - 1}-|"; }
                        else { BoardMatrix[rowIndex, colIndex] = $"|{colIndex - 1}|"; }
                    }
                    if (colIndex == 1 || colIndex == Width - 2)
                    {
                        BoardMatrix[rowIndex, colIndex] = "|--|";
                    }
                    if (rowIndex == 1 || rowIndex == Height - 2)
                    {
                        BoardMatrix[rowIndex, colIndex] = "|--|";
                    }
                    if ((rowIndex < 2 || rowIndex > Height - 3) && (colIndex < 2 || colIndex > Width - 3))
                    {
                        BoardMatrix[rowIndex, colIndex] = "|++|";
                    }
                }
            }
        }

        public static void CreateBoard()
        {
            CurrentBoard.FillBoardMatrix();
            DrawBoard();
            NextTurn();
            WhoWins();
        }

        public static void DrawBoard()
        {
            for (int rowIndex = 0; rowIndex < (CurrentBoard.BoardMatrix.GetUpperBound(0) + 1); rowIndex++)
            {
                for (int colIndex = 0; colIndex < CurrentBoard.BoardMatrix.GetUpperBound(1) + 1; colIndex++)
                {
                    Console.Write(CurrentBoard.BoardMatrix[rowIndex, colIndex]);
                }
                Console.WriteLine();
            }
        }

        public static void FillTheCells(Player player)
        {
            for (var rowIndex = 0; rowIndex < (CurrentBoard.BoardMatrix.GetUpperBound(0) + 1); rowIndex++)
            {
                for (var colIndex = 0; colIndex < CurrentBoard.BoardMatrix.GetUpperBound(1) + 1; colIndex++)
                {
                    if (rowIndex >= CurrentPoint.Y + 1
                     && rowIndex < CurrentPoint.Y + 1 + CurrentDices.FirstDice
                      && colIndex >= CurrentPoint.X + 1
                       && colIndex < CurrentPoint.X + 1 + CurrentDices.SecondDice)
                    {
                        CurrentBoard.BoardMatrix[rowIndex, colIndex] = player.Filler;
                    }
                    Console.Write(CurrentBoard.BoardMatrix[rowIndex, colIndex]);
                }
                Console.WriteLine();
            }
            var filledCells = CurrentDices.FirstDice * CurrentDices.SecondDice;
            CurrentBoard.EmptyCellsCount -= filledCells;
            player.FilledCells += filledCells;
            Console.WriteLine($"{player.Name}, you have already filled {player.FilledCells} cells.");
        }

        public static void GetnerateCoordinates()
        {
            CurrentPoint = new(1, CurrentBoard.Cols, CurrentBoard.Rows);
            if (!IsFieldFitToBoard(CurrentPoint.X, CurrentPoint.Y) || IsFieldOverlapAnotherField(CurrentPoint.X, CurrentPoint.Y))
            {
                GetnerateCoordinates();
            }
        }

        public static void GetСoordinates()
        {
            CurrentPoint = new(ValueValidation("coordinate X", 1, CurrentBoard.Cols), ValueValidation("coordinate Y", 1, CurrentBoard.Rows));
            if (!IsFieldFitToBoard(CurrentPoint.X, CurrentPoint.Y))
            {
                Console.WriteLine("Cannot be drawn from this point - the field will not fit!");
                GetСoordinates();
            }
            else if (IsFieldOverlapAnotherField(CurrentPoint.X, CurrentPoint.Y))
            {
                Console.WriteLine("Cannot be drawn there, cells is already occupied!");
                GetСoordinates();
            }
        }

        public static bool IsFieldFitToBoard(int x, int y)
        {
            return y + CurrentDices.FirstDice - 1 <= CurrentBoard.Rows
             && x + CurrentDices.SecondDice - 1 <= CurrentBoard.Cols;
        }

        public static bool IsFieldOverlapAnotherField(int x, int y)
        {
            for (var rowIndex = y + 1; rowIndex < y + 1 + CurrentDices.FirstDice; rowIndex++)
            {
                for (var colIndex = x + 1; colIndex < x + 1 + CurrentDices.SecondDice; colIndex++)
                {
                    if (!CurrentBoard.BoardMatrix[rowIndex, colIndex].Equals(CurrentBoard.Filler))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsPlacementFieldOnBooardPossible()
        {
            for (var rowBoardIndex = borderSize; rowBoardIndex < (CurrentBoard.BoardMatrix.GetUpperBound(0) - borderSize); rowBoardIndex++)
            {
                for (var colBoardIndex = borderSize; colBoardIndex < CurrentBoard.BoardMatrix.GetUpperBound(1) - borderSize; colBoardIndex++)
                {
                    if (CurrentBoard.BoardMatrix[rowBoardIndex, colBoardIndex].Equals(CurrentBoard.Filler)
                     && IsFieldFitToBoard(colBoardIndex - 1, rowBoardIndex - 1)
                     && !IsFieldOverlapAnotherField(colBoardIndex - 1, rowBoardIndex - 1))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}