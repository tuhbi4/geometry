﻿using System;

namespace Geometry
{
    public static class Validator
    {
        public static int NumberValidation()
        {
            if (!int.TryParse(Console.ReadLine(), out int number))
            {
                Console.WriteLine("This is not a number. Please enter the correct answer:");
                return NumberValidation();
            }
            return number;
        }

        public static int ValueValidation(string valueName, int minValue, int maxValue)
        {
            int valueOut;
            Console.WriteLine($"Set the number of {valueName}: (minimum is: {minValue}; maximum is {maxValue})");
            while (true)
            {
                valueOut = NumberValidation();
                if (valueOut < minValue)
                {
                    Console.WriteLine($"Please enter a value greater than or equal to {minValue}");
                }
                else if (valueOut > maxValue)
                {
                    Console.WriteLine($"Please enter a value less than or equal to {maxValue}");
                }
                else
                {
                    return valueOut;
                }
            }
        }
    }
}