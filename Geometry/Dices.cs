﻿using System;
using static Geometry.Board;
using static Geometry.Game;

namespace Geometry
{
    public class Dices
    {
        public int FirstDice { get; set; }
        public int SecondDice { get; set; }

        public Dices(int firstDice, int secondDice)
        {
            FirstDice = firstDice;
            SecondDice = secondDice;
        }

        public void RollTheDices()
        {
            Random diceRandomValue = new();
            FirstDice = diceRandomValue.Next(1, 6);
            SecondDice = diceRandomValue.Next(1, 6);
        }

        public static void RollingDices(Player player)
        {
            Console.WriteLine("Throwing the dices ...");
            CurrentDices.RollTheDices();
            Console.WriteLine("+-------+       +-------+");
            Console.WriteLine("|\\       \\     /       /|");
            Console.WriteLine($"| +-------+   +-------+ |\n| |       |   |       | | \n+ |   {CurrentDices.FirstDice}   |   |   {CurrentDices.SecondDice}   | +");
            Console.WriteLine(" \\|       |   |       |/ ");
            Console.WriteLine("  +-------+   +-------+  ");

            if (!IsPlacementFieldOnBooardPossible())
            {
                Console.WriteLine("Oops, it seems like you can't draw a rectangle with dices like this.");
                if (player.AttemptsToRoll == 0)
                {
                    Console.WriteLine("Unfortunately, you're out of luck again. You have no more attempts to throw the dices...");
                    player.GameOver = true;
                    Console.WriteLine($"Don't worry {player.Name}, next time you will be more fortunate!");
                }
                else
                {
                    Console.WriteLine($"You have {player.AttemptsToRoll} more attempts...");
                    player.AttemptsToRoll--;
                    RollingDices(player);
                }
            }
        }
    }
}