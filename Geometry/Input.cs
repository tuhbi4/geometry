﻿using System;
using static Geometry.Game;
using static Geometry.Validator;

namespace Geometry
{
    public static class Input
    {
        public static int RequestGameMode()
        {
            int modeSelector;
            Console.WriteLine("Do you want to play against another player or the computer?\n1. Vs Player\n2. Vs Computer");
            while (true)
            {
                modeSelector = NumberValidation();
                if (modeSelector == 1 || modeSelector == 2)
                {
                    break;
                }
            }
            return modeSelector;
        }

        public static void RequestBoardSize()
        {
            Console.WriteLine("Please set the size of the field. (mininum is: 20x30)");
            var cols = ValueValidation("columns", 20, 38);
            var rows = ValueValidation("rows", 30, 99);
            CurrentBoard = new(rows, cols, "    ");
        }

        public static void RequestNumberOfTurns()
        {
            CurrentTurns = new(ValueValidation("moves for the players ", 20, 3762));
        }

        public static string RequestName(string defaultName)
        {
            string name;
            Console.WriteLine($"{defaultName}, enter your name or leave the field blank:");
            name = Console.ReadLine();
            if (name.Length == 0)
            {
                return defaultName;
            }
            return name;
        }

        public static string RequestFiller(string defaultFiller)
        {
            string pattern;
            Console.WriteLine($"Your default pattern is {defaultFiller}. Enter a new one or leave the field blank:");
            pattern = Console.ReadLine();
            if (pattern.Length == 0)
            {
                return defaultFiller;
            }
            else if (pattern.Length != defaultFiller.Length)
            {
                Console.WriteLine("The pattern must have 4 characters. Enter again:");
                return RequestFiller(defaultFiller);
            }
            else if (pattern.Equals("    "))
            {
                Console.WriteLine("The pattern cannot be the same as the board filler. Enter again:");
                return RequestFiller(defaultFiller);
            }
            else if (Players.Count != 0)
            {
                foreach (var player in Players)
                {
                    if (pattern.Equals(player.Filler))
                    {
                        Console.WriteLine("This template is already taken. Enter again:");
                        return RequestFiller(defaultFiller);
                    }
                }
            }
            return pattern;
        }
    }
}