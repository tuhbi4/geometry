﻿using System;

namespace Geometry
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int coordinateX, int coordinateY)
        {
            X = coordinateX;
            Y = coordinateY;
        }

        public Point(int startOn, int xEndOn, int yEndOn)
        {
            Random randomCoordinate = new();
            X = randomCoordinate.Next(startOn, xEndOn);
            Y = randomCoordinate.Next(startOn, yEndOn);
        }
    }
}