﻿using System;
using System.Collections.Generic;
using static Geometry.Board;
using static Geometry.Dices;
using static Geometry.Input;

namespace Geometry
{
    public class Game
    {
        public string GameMode { get; set; }
        public static string Winner { get; set; }
        public static bool GameOver { get; set; }
        public static List<Player> Players { get; set; } = new() { };
        public static Board CurrentBoard { get; set; }
        public static Turns CurrentTurns { get; set; }
        public static Dices CurrentDices { get; set; } = new(1, 1);
        public static Point CurrentPoint { get; set; }

        private readonly List<string> gameModes = new()
        {
            "Player Vs Player",
            "Player Vs Computer"
        };

        public void StartGame()
        {
            StartGame(RequestGameMode(), false, false, false);
        }

        public void StartGame(int gameMode, bool allDefault)
        {
            if (allDefault)
            {
                StartGame(gameMode, true, true, true);
            }
            else
            {
                StartGame(gameMode, false, false, false);
            }
        }

        public void StartGame(int gameMode, bool defaultPlayersNames, bool defaultBoardSize, bool defaultTurnsCount)
        {
            GameMode = gameModes[gameMode - 1];
            if (defaultPlayersNames)
            {
                Players.Add(new Player("Player 1", "####"));
                if (GameMode.Equals(gameModes[0]))
                {
                    Players.Add(new Player("Player 2", "****"));
                }
                else
                {
                    Players.Add(new Player("Computer", "%%%%"));
                }
            }
            else
            {
                IdentifyPlayersNames();
            }
            if (defaultBoardSize)
            {
                CurrentBoard = new(30, 20, "    ");
            }
            else
            {
                RequestBoardSize();
            }
            if (defaultTurnsCount)
            {
                CurrentTurns = new(20);
            }
            else
            {
                RequestNumberOfTurns();
            }
            CreateBoard();
        }

        public void StartGame(int gameMode, string nameFirstPlayer, string patternFirstPlayer, string nameSecondPlayer, string patternSecondPlayer, int cols, int rows, int turns)
        {
            GameMode = gameModes[gameMode - 1];
            Players.Add(new(nameFirstPlayer, patternFirstPlayer));
            if (GameMode.Equals(gameModes[0]))
            {
                Players.Add(new(nameSecondPlayer, patternSecondPlayer));
            }
            else
            {
                Players.Add(new("Computer", patternSecondPlayer));
            }
            CurrentBoard = new(rows, cols, "    ");
            CurrentTurns = new(turns);
            CreateBoard();
        }

        public void IdentifyPlayersNames()
        {
            Players.Add(new Player(RequestName("Player 1"), RequestFiller("####")));
            if (GameMode.Equals(gameModes[0]))
            {
                Players.Add(new Player(RequestName("Player 2"), RequestFiller("****")));
            }
            else if (GameMode.Equals(gameModes[1]))
            {
                Console.WriteLine("Your opponent is Computer!");
                if (Players[0].Filler.Equals("%%%%"))
                {
                    Players.Add(new("Computer", "####"));
                }
                else
                {
                    Players.Add(new("Computer", "%%%%"));
                }
            }
        }

        public static void NextTurn()
        {
            while (!GameOver)
            {
                CurrentTurns.CurrentTurnNumber++;
                if (CurrentTurns.CurrentTurnNumber == 1)
                {
                    Console.WriteLine($"\nGame started! Current turn: {CurrentTurns.CurrentTurnNumber}.");
                }
                else
                {
                    Console.WriteLine($"\nCurrent turn: {CurrentTurns.CurrentTurnNumber}. Turns are left: {CurrentTurns.TurnsCount - CurrentTurns.CurrentTurnNumber}.");
                }
                foreach (Player player in Players)
                {
                    Console.WriteLine($"\n{player.Name}, your turn!");
                    if (player.Name.Equals("Computer"))
                    {
                        ComputerTurn(player);
                    }
                    else
                    {
                        PlayerTurn(player);
                    }
                    if (!player.GameOver)
                    {
                        FillTheCells(player);
                    }
                }
                IsGameOver();
            }
        }

        public static void PlayerTurn(Player player)
        {
            RollingDices(player);
            if (!player.GameOver)
            {
                Console.WriteLine("Please enter the coordinates of the drawing start point:");
                GetСoordinates();
            }
        }

        public static void ComputerTurn(Player player)
        {
            RollingDices(player);
            if (!player.GameOver)
            {
                Console.WriteLine("Computer selects a point...");
                GetnerateCoordinates();
                Console.WriteLine($"Computer drew from [{CurrentPoint.X},{CurrentPoint.Y}].");
            }
        }

        public static void IsGameOver()
        {
            if (Players[0].GameOver && Players[1].GameOver)
            {
                GameOver = true;
                SetWinner();
            }
            if (CurrentBoard.EmptyCellsCount < 1)
            {
                Console.WriteLine("There are no more free cells on the field...");
                GameOver = true;
                SetWinner();
            }
            if (CurrentTurns.CurrentTurnNumber == CurrentTurns.TurnsCount)
            {
                Console.WriteLine("Players, your moves are over...");
                GameOver = true;
                SetWinner();
            }
        }

        public static void SetWinner()
        {
            if (Players[0].FilledCells > Players[1].FilledCells)
            {
                Winner = Players[0].Name;
            }
            else
            {
                Winner = Players[1].Name;
            }
        }

        public static void WhoWins()
        {
            Console.WriteLine($"\n*****GAME OVER!*****\nCongratulations {Winner}, you wins!");
        }
    }
}