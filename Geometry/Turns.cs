﻿namespace Geometry
{
    public class Turns
    {
        public int TurnsCount { get; }
        public int CurrentTurnNumber { get; set; }

        public Turns(int count)
        {
            TurnsCount = count;
            CurrentTurnNumber = 0;
        }

        public bool IsTurnsCountOver()
        {
            if (CurrentTurnNumber == TurnsCount)
            {
                return true;
            }
            return false;
        }
    }
}